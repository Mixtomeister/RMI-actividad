/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad1pdsp;

/**
 *
 * @author Mixtomeister
 */
public class FileCell {
    
    private final String file;
    private String status;

    public FileCell(String file) {
        this.file = file;
        this.status = "Unknown";
    }

    public String getFile() {
        return file;
    }

    public String getStatus() {
        return status;
    }
    
    public void setStatusFound(){
        this.status = "Found";
    }
    
    public void setStatusNotFound(){
        this.status = "Not found";
    }
    
}
