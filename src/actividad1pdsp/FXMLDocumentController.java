/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package actividad1pdsp;

import filebrowser.FileBrowser;
import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.DirectoryChooser;

/**
 *
 * @author Mixtomeister
 */
public class FXMLDocumentController implements Initializable {
    @FXML
    private Button btnFolderChooser;
    @FXML
    private Button btnAdd;
    @FXML
    private Button btnSearch;
    @FXML
    private Button btnClear;
    @FXML
    private TextField txt_dir;
    @FXML
    private TextField txt_file;
    @FXML
    private TableView<FileCell> tableView;
    @FXML
    private TableColumn<FileCell, String> columnFile;
    @FXML
    private TableColumn<FileCell, String> columnStatus;
    
    private DirectoryChooser directoryChooser;
    private FileBrowser fileBrowser;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        this.directoryChooser = new DirectoryChooser();
        this.fileBrowser = new FileBrowser();
        
        this.columnFile.setCellValueFactory(new PropertyValueFactory("file"));
        this.columnStatus.setCellValueFactory(new PropertyValueFactory("status"));

        this.btnFolderChooser.setOnMouseClicked(e -> this.selectFolder());
        this.btnAdd.setOnMouseClicked(e -> this.addFileToList());
        this.btnClear.setOnMouseClicked(e -> this.tableView.getItems().clear());
        this.btnSearch.setOnMouseClicked(e -> this.searchAllFiles());
    }
    
    private void selectFolder(){
        File folder = this.directoryChooser.showDialog(MainWindow.stage);
        if(folder != null){
            if(this.fileBrowser.setDirectory(folder.getAbsolutePath())){
                this.txt_dir.setText(folder.getAbsolutePath());
            }else{
                this.error("Error", "The directory was not found.");
            }
        }
    }
    
    private void addFileToList(){
        if(!this.txt_file.getText().equals("")){
            this.tableView.getItems().add(new FileCell(this.txt_file.getText()));
            this.txt_file.setText("");
        }else{
            this.error("Error", "Unable to search for an unnamed file.");
        }
    }
    
    private void searchAllFiles(){
        if(!this.tableView.getItems().isEmpty()){
            this.tableView.getItems().forEach((cell) -> {
                if(this.fileBrowser.searchFile(cell.getFile())){
                    cell.setStatusFound();
                }else{
                    cell.setStatusNotFound();
                }
            });
            this.tableView.refresh();
        }else{
            this.error("Error", "There is no file to search.");
        }
    }
    
    private void error(String title, String body) {
        Alert alert = new Alert(AlertType.ERROR);
        alert.setTitle(title);
        alert.setHeaderText(null);
        alert.setContentText(body);
        alert.showAndWait();
    }
}
