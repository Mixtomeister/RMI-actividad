/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package filebrowser;

import java.util.Properties;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author Mixtomeister
 */
public class FileBrowser {
    private final Properties p;
    private Context ctx;
    
    public FileBrowser() {
        this.p = new Properties();
        this.p.put(Context.INITIAL_CONTEXT_FACTORY, "com.sun.jndi.fscontext.RefFSContextFactory");
    }
    
    public boolean setDirectory(String url){
        this.p.put(Context.PROVIDER_URL, "file:" + url);
        try {
            this.ctx = new InitialContext(p);
            return true;
        } catch (NamingException ex) {
            return false;
        }
    }
    
    public boolean searchFile(String file){
        try {
            this.ctx.lookup(file);
            return true;
        } catch (NamingException ex) {
            return false;
        }
    }
}